export default {
    MAX_ATTACHMENT_SIZE: 5000000,
    s3: {
      REGION: "us-east-1",
      BUCKET: "ffr-blog-app-uploads"
    },
    apiGateway: {
      REGION: "us-east-1",
      URL: "https://26yx7tuyd0.execute-api.us-east-1.amazonaws.com/dev"
    },
    cognito: {
      REGION: "us-east-1",
      USER_POOL_ID: "us-east-1_fyaaAaH6X",
      APP_CLIENT_ID: "7bmd8ec7e2109bu6gaonge9kqa",
      IDENTITY_POOL_ID: "us-east-1:983a2067-1e72-41af-a0dc-f68c9053a6ab"
    }
  };